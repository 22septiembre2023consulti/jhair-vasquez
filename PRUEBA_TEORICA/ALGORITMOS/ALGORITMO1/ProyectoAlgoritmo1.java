package PRUEBA_TEORICA.ALGORITMOS.ALGORITMO1;

import java.util.Scanner;

public class ProyectoAlgoritmo1 {

    public void algoritmo1() {
        try (Scanner consoleInput = new Scanner(System.in)) {
            String frase, palabra;
            System.out.print("Ingrese una frase: ");
            frase = consoleInput.nextLine();
            String reversedPrhase = reversePhrase(frase);
            System.out.print("Ingrese una palabra: ");
            palabra = consoleInput.nextLine();

            String result = reversedPrhase.replace(" ", palabra);

            System.out.println(result);
        } catch (Exception e) {
            System.out.println("Por favor ingrese una palabra valida.");
        }

    }

    public String reversePhrase(String prhase) {
        String reversedPrhase = "";

        for (int i = 0; i < prhase.length(); i++) {
            reversedPrhase = prhase.charAt(i) + reversedPrhase;
        }

        return reversedPrhase;
    }

    public static void main(String[] args) {
        ProyectoAlgoritmo1 proyectoAlgoritmo1 = new ProyectoAlgoritmo1();

        proyectoAlgoritmo1.algoritmo1();
    }

}